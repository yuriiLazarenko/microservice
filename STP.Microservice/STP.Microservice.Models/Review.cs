﻿using System;
using STP.Microservice.Models.Base;

namespace STP.Microservice.Models
{
	public class Review:Auditable,IVisible
	{
		public string Content { get; set; }
		public string UserName { get; set; }
		public int Rating { get; set; }
		public  Guid ProductId { get; set; }
		public  virtual Product Product { get; set; }


		public bool Visible { get; set; }
	}
}