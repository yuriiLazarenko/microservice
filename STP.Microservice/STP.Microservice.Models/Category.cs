﻿

using System.Collections;
using System.Collections.Generic;
using STP.Microservice.Models.Base;

namespace STP.Microservice.Models
{
	public class Category : Identity, IVisible
	{
		public  string Name { get; set; }

		public  string Description { get; set; }

		public virtual ICollection<Product> Products { get; set; }
		public bool Visible { get; set; }
	}
}
