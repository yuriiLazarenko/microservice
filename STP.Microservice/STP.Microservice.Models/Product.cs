﻿

using System;
using System.Collections.Generic;
using STP.Microservice.Models.Base;

namespace STP.Microservice.Models
{
	public class Product : Auditable, IVisible
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public Guid CategoryId { get; set; }
		public int Price { get; set; }
		public virtual  Category Category { get; set; }

		public virtual  ICollection<Review> Reviews { get; set; }
		public virtual  ICollection<ProductTag> ProductTags { get; set; }
		public bool Visible { get; set; }
	}
}
