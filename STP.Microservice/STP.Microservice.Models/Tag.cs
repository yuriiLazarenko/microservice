﻿using System.Collections.Generic;
using STP.Microservice.Models.Base;

namespace STP.Microservice.Models
{
	public class Tag:Identity
	{
		public string Name { get; set; }

		public virtual ICollection<ProductTag> ProductTags { get; set; }
	}
}