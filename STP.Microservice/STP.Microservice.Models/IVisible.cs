﻿namespace STP.Microservice.Models
{
	public interface IVisible
	{
		 bool Visible { get; set; }
	}
}