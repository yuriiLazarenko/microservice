﻿using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using STP.Microservice.Models;

namespace STP.Microservice.Data.ModelConfigurations
{
	public class ProductTagModelConfiguration:IEntityTypeConfiguration<ProductTag>
	{
		public void Configure(EntityTypeBuilder<ProductTag> builder)
		{
			builder.HasKey(x => new {x.ProductId, x.TagId});

			builder.HasOne(x => x.Product)
				.WithMany(x => x.ProductTags)
				.HasForeignKey(x => x.ProductId);

			builder.HasOne(x => x.Tag)
				.WithMany(x => x.ProductTags)
				.HasForeignKey(x => x.TagId);
		}
	}
}