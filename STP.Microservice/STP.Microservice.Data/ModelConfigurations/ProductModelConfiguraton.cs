﻿

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using STP.Microservice.Data.ModelConfigurations.Base;using STP.Microservice.Models;namespace STP.Microservice.Data.ModelConfigurations
{
	public class ReviewModelConfiguraton : AuditableModelConfigurationBase<Review>
	{
		protected override void AddBuilder(EntityTypeBuilder<Review> builder)
		{
			builder.Property(x => x.UserName).HasMaxLength(128).IsRequired();
			builder.Property(x => x.Content).HasMaxLength(1000);
			builder.Property(x => x.Rating);
			builder.Property(x => x.ProductId).IsRequired();
			builder.Property(x => x.Visible);

			builder.HasOne(x => x.Product);
		}

		protected override string TableName()
		{
			return "Reviews";
		}
	}
}
