﻿

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using STP.Microservice.Data.ModelConfigurations.Base;using STP.Microservice.Models;namespace STP.Microservice.Data.ModelConfigurations
{
	public class TagModelConfiguraton : IdentityModelConfigurationBase<Tag>
	{
		protected override void AddBuilder(EntityTypeBuilder<Tag> builder)
		{
			builder.Property(x => x.Name).HasMaxLength(50).IsRequired();
			
		}

		protected override string TableName()
		{
			return "Tags";
		}
	}
}
