﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using STP.Microservice.Data.ModelConfigurations.Base;
using STP.Microservice.Models;

namespace STP.Microservice.Data.ModelConfigurations
{
	public class CategoryModelConfiguration : IdentityModelConfigurationBase<Category>
	{
		protected override void AddBuilder(EntityTypeBuilder<Category> builder)
		{
			builder.Property(x => x.Name).HasMaxLength(50).IsRequired();
			builder.Property(x => x.Description).HasMaxLength(1000);
			builder.Property(x => x.Visible);

			builder.HasMany(x => x.Products);
		}

		protected override string TableName()
		{
			return "Categories";
		}
	}
}
