﻿

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using STP.Microservice.Data.ModelConfigurations.Base;using STP.Microservice.Models;namespace STP.Microservice.Data.ModelConfigurations
{
	public class ProductModelConfiguraton : AuditableModelConfigurationBase<Product>
	{
		protected override void AddBuilder(EntityTypeBuilder<Product> builder)
		{
			builder.Property(x => x.Name).HasMaxLength(128).IsRequired();
			builder.Property(x => x.Description).HasMaxLength(1000);
			builder.Property(x => x.Price);
			builder.Property(x => x.CategoryId).IsRequired();
			builder.Property(x => x.Visible);

			builder.HasOne(x => x.Category);
		}

		protected override string TableName()
		{
			return "Products";
		}
	}
}
