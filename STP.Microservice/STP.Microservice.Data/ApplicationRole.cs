﻿using System;
using Microsoft.AspNetCore.Identity;

namespace STP.Microservice.Data
{
    /// <summary>
    /// Application role
    /// </summary>
    public class ApplicationRole : IdentityRole<Guid>
    {
    }
}
