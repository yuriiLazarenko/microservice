﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using STP.Microservice.Models;

namespace STP.Microservice.Web.Infrastructure.ViewModels.CategoryViewModels
{
	public class CategoryUpdateViewModel: ViewModelBase,IValidatableObject,IVisible
	{ 
		/// <summary>
		/// Name of the catalog
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Description for current catalog
		/// </summary>
		public string Description { get; set; }

		public bool Visible { get; set; }

		public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (string.IsNullOrEmpty(Name))
			{
				yield return new ValidationResult("Name should be not empty");
			}
			if (!string.IsNullOrEmpty(Name) && (Name.Length < 5))
			{
				yield return new ValidationResult("Name should contain more than 5 symbols");
			}
		}

	}
}