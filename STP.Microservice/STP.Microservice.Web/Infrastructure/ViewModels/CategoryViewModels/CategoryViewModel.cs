﻿namespace STP.Microservice.Web.Infrastructure.ViewModels.CategoryViewModels
{
	public class CategoryViewModel:ViewModelBase
	{
		public string Name { get; set; }

		public string Description { get; set; }
	}
}