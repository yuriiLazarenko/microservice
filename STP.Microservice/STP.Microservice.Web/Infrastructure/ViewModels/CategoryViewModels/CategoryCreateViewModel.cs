﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using STP.Microservice.Web.Infrastructure.Factories.Base;

namespace STP.Microservice.Web.Infrastructure.ViewModels.CategoryViewModels
{
	public class CategoryCreateViewModel:IViewModel, IValidatableObject
	{
		public string Name { get; set; }

		public string Description { get; set; }
		public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (string.IsNullOrEmpty(Name))
			{
				yield return new ValidationResult("Name should be not empty");
			}
			if (!string.IsNullOrEmpty(Name)&&(Name.Length<5))
			{
				yield return new ValidationResult("Name should contain more than 5 symbols");
			}
		}
	}
}