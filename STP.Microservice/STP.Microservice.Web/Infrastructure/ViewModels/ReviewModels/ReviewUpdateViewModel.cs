﻿using System;
using STP.Microservice.Models;

namespace STP.Microservice.Web.Infrastructure.ViewModels.ReviewModels
{
	/// <summary>
	/// ViewModel for Review updating
	/// </summary>
	public class ReviewUpdateViewModel : ViewModelBase,IVisible
	{

		public string Content { get; set; }
		public string UserName { get; set; }
		public int Rating { get; set; }
		public Guid ProductId { get; set; }
		public bool Visible { get; set; }
	}
}