﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using STP.Microservice.Core;
using STP.Microservice.Web.Infrastructure.Factories.Base;
using STP.Microservice.Web.Infrastructure.ViewModels.ProductViewModels;

namespace STP.Microservice.Web.Infrastructure.ViewModels.ReviewModels
{
	/// <summary>
	/// ViewModel for Review creation
	/// </summary>
	public class ReviewCreateViewModel : IViewModel, IValidatableObject
	{
		public string Content { get; set; }
		public string UserName { get; set; }
		public int Rating { get; set; }
		public Guid ProductId { get; set; }
		public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (string.IsNullOrEmpty(UserName))
			{
				yield return new ValidationResult(AppData.Messages.UserNameRequire);
			}

			if (string.IsNullOrEmpty(Content))
			{
				yield return new ValidationResult(AppData.Messages.ReviewContentRequired);
			}

			if (Rating<1)
			{
				yield return  new ValidationResult(AppData.Messages.RatingMinRequired);
			}

			if (Rating>5)
			{
				yield return  new ValidationResult(AppData.Messages.RatingMaxRequired);
			}
		}
	}
}