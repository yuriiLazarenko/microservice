﻿using System;
using STP.Microservice.Models;
using STP.Microservice.Web.Infrastructure.ViewModels.ProductViewModels;

namespace STP.Microservice.Web.Infrastructure.ViewModels.ReviewModels
{
	/// <summary>
	/// ViewModel for Review UI viewing
	/// </summary>
	public class ReviewViewModel : ViewModelBase
	{
		public string Content { get; set; }
		public string UserName { get; set; }
		public int Rating { get; set; }
		public Guid ProductId { get; set; }
		public virtual ProductViewModel Product  { get; set; }
	}
}