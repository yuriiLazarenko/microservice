﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using STP.Microservice.Models;

namespace STP.Microservice.Web.Infrastructure.ViewModels.ProductViewModels
{
	/// <summary>
	/// ViewModel for Product updating
	/// </summary>
	public class ProductUpdateViewModel : ViewModelBase,IVisible,IValidatableObject
	{

		public string Name { get; set; }
		public string Description { get; set; }
		public Guid CategoryId { get; set; }
		public int Price { get; set; }
		public bool Visible { get; set; }
		public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (string.IsNullOrEmpty(Name))
			{
				yield return new ValidationResult("Name should be not empty");
			}
			if (!string.IsNullOrEmpty(Name) && (Name.Length < 1))
			{
				yield return new ValidationResult("Name should contain more than 1 symbols");
			}
		}
	}
}