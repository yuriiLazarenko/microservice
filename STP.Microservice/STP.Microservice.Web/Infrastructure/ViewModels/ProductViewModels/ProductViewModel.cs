﻿using System;
using STP.Microservice.Models;

namespace STP.Microservice.Web.Infrastructure.ViewModels.ProductViewModels
{
	/// <summary>
	/// ViewModel for Product UI viewing
	/// </summary>
	public class ProductViewModel : ViewModelBase,IVisible
	{

		public string Name { get; set; }
		public string Description { get; set; }
		public Guid CategoryId { get; set; }
		public int Price { get; set; }
		public string CategoryName { get; set; }

		public bool Visible { get; set; }
	}
}