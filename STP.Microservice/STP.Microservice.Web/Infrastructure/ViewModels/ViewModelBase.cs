﻿using System;
using STP.Microservice.Models.Base;
using STP.Microservice.Web.Infrastructure.Factories.Base;

namespace STP.Microservice.Web.Infrastructure.ViewModels
{
    /// <summary>
    /// ViewModelBase for WritableController
    /// </summary>
    public class ViewModelBase : IViewModel, IHaveId
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public Guid Id { get; set; }
    }
}
