﻿namespace STP.Microservice.Web.Infrastructure.QueryParams
{
	public class DefaultPagedListQueryParams: PagedListQueryParams
	{
		public DefaultPagedListQueryParams()
		{
			PageSize = 10;
		}
	}
}