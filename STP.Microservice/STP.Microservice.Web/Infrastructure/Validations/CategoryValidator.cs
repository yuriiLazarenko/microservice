﻿using System.Collections;
using System.Collections.Generic;
using Calabonga.EntityFrameworkCore.UnitOfWork;
using STP.Microservice.Data;
using STP.Microservice.Models;
using STP.Microservice.Web.Infrastructure.Validations.Base;

namespace STP.Microservice.Web.Infrastructure.Validations
{
	public class CategoryValidator : EntityValidator<Category>
	{
		private readonly IUnitOfWork<ApplicationDbContext, ApplicationUser, ApplicationRole> _unitOfWork;

		public CategoryValidator(IUnitOfWork<ApplicationDbContext, ApplicationUser, ApplicationRole> unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}
		/// <inheritdoc />
		public override IEnumerable<ValidationResult> ValidateOnInsert (Category entity)
		{
			var category = _unitOfWork.GetRepository<Category>()
				.GetFirstOrDefault(predicate: x => x.Name.ToLower().Equals(entity.Name.ToLower()));
			if (category!=null)
			{
				yield return new ValidationResult("Category with the name {entity.Name} already exists");
			}
		}
	}
}