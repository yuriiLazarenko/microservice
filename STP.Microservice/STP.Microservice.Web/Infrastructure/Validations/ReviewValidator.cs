﻿using System.Collections.Generic;
using STP.Microservice.Models;
using STP.Microservice.Web.Infrastructure.Validations.Base;

namespace STP.Microservice.Web.Infrastructure.Validations
{
	/// <summary>
	/// Entity Validator for Review
	/// </summary>
	public class ReviewValidator : EntityValidator<Review>
	{
		/// <inheritdoc />
		public override IEnumerable<ValidationResult> ValidateOnInsert(Review entity)
		{
			return base.ValidateOnInsert(entity);
		}

		/// <inheritdoc />
		public override IEnumerable<ValidationResult> ValidateOnInsertOrUpdate(Review entity)
		{
			return base.ValidateOnInsertOrUpdate(entity);
		}

		/// <inheritdoc />
		public override IEnumerable<ValidationResult> ValidateOnUpdate(Review entity)
		{
			return base.ValidateOnUpdate(entity);
		}
	}
}