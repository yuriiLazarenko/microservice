﻿using STP.Microservice.Models;
using STP.Microservice.Web.Infrastructure.Validations.Base;

namespace STP.Microservice.Web.Infrastructure.Validations
{
	/// <summary>
	/// Entity Validator for Product
	/// </summary>
	public class ProductValidator : EntityValidator<Product>
	{
	
	}
}