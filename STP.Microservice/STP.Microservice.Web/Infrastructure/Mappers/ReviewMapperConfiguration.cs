﻿using Calabonga.EntityFrameworkCore.UnitOfWork;
using STP.Microservice.Models;
using STP.Microservice.Web.Extensions;
using STP.Microservice.Web.Infrastructure.Mappers.Base;
using STP.Microservice.Web.Infrastructure.ViewModels.ReviewModels;

namespace STP.Microservice.Web.Controllers
{
	/// <summary>
	/// Mapper Configuration for entity Review
	/// </summary>
	public class ReviewMapperConfiguration : MapperConfigurationBase
	{
		/// <inheritdoc />
		public ReviewMapperConfiguration()
		{
			CreateMap<Review, ReviewViewModel>();

			CreateMap<ReviewCreateViewModel, Review>()
				.ForMember(x => x.Id, o=>o.Ignore())
				.ForMember(x => x.Visible, o=>o.MapFrom(_=>false))
				.ForMember(x => x.Product, o=>o.Ignore())
				.IgnoreAudit();

			CreateMap<ReviewUpdateViewModel, Review>()
				.ForMember(x => x.Product, o => o.Ignore())
				.IgnoreAudit();

			CreateMap<Review, ReviewUpdateViewModel>()
				.ForAllMembers(x => x.Ignore());

			CreateMap<IPagedList<Review>, IPagedList<ReviewViewModel>>()
				.ConvertUsing<PagedListConverter<Review, ReviewViewModel>>();
		}
	}
}