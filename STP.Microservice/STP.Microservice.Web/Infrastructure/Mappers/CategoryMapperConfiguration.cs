﻿using Calabonga.EntityFrameworkCore.UnitOfWork;
using STP.Microservice.Models;
using STP.Microservice.Web.Controllers;
using STP.Microservice.Web.Infrastructure.Mappers.Base;
using STP.Microservice.Web.Infrastructure.ViewModels.CategoryViewModels;
using STP.Microservice.Web.Infrastructure.ViewModels.LogViewModels;

namespace STP.Microservice.Web.Infrastructure.Mappers
{
	/// <summary>
	/// // Calabonga: update summary (2019-05-26 12:44 CategoryMapperConfiguration)
	/// </summary>
	public class CategoryMapperConfiguration : MapperConfigurationBase
	{
		/// <inheritdoc />
		public CategoryMapperConfiguration()
		{
			CreateMap<Category, CategoryViewModel>();

			CreateMap<Category, CategoryUpdateViewModel>();

			CreateMap<CategoryUpdateViewModel, Category>()
				.ForMember(x => x.Products, o => o.Ignore());

			CreateMap<CategoryCreateViewModel, Category>()
				.ForMember(x => x.Id, o => o.Ignore())
				.ForMember(x => x.Visible, o => o.MapFrom(_=>false))
				.ForMember(x => x.Products, o => o.Ignore());

			CreateMap<IPagedList<Category>, IPagedList<CategoryViewModel>>()
				.ConvertUsing<PagedListConverter<Category, CategoryViewModel>>();
		}
	}
}