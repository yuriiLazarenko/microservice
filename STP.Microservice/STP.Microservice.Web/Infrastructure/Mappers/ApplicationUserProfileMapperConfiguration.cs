﻿using STP.Microservice.Data;
using STP.Microservice.Web.Infrastructure.Mappers.Base;
using STP.Microservice.Web.Infrastructure.ViewModels.AccountViewModels;

namespace STP.Microservice.Web.Infrastructure.Mappers
{
    /// <summary>
    /// Mapper Configuration for entity Person
    /// </summary>
    public class ApplicationUserProfileMapperConfiguration : MapperConfigurationBase
    {
        /// <inheritdoc />
        public ApplicationUserProfileMapperConfiguration()
        {
            CreateMap<RegisterViewModel, ApplicationUserProfile>()
                .ForAllOtherMembers(x => x.Ignore());
        }
    }
}