﻿using Calabonga.EntityFrameworkCore.UnitOfWork;
using STP.Microservice.Models;
using STP.Microservice.Web.Controllers;
using STP.Microservice.Web.Extensions;
using STP.Microservice.Web.Infrastructure.Mappers.Base;
using STP.Microservice.Web.Infrastructure.ViewModels.ProductViewModels;

namespace STP.Microservice.Web.Infrastructure.Mappers
{
	/// <summary>
	/// Mapper Configuration for entity Product
	/// </summary>
	public class ProductMapperConfiguration : MapperConfigurationBase
	{
		/// <inheritdoc />
		public ProductMapperConfiguration()
		{
			CreateMap<Product, ProductViewModel>();

			CreateMap<ProductCreateViewModel, Product>()
				.ForMember(x => x.Visible, o=>o.MapFrom(_=>false))
				.ForMember(x => x.Id, o=>o.Ignore())
				.ForMember(x => x.ProductTags, o=>o.Ignore())
				.ForMember(x => x.Reviews, o=>o.Ignore())
				.ForMember(x => x.Category, o=>o.Ignore())
				.IgnoreAudit();

			CreateMap<ProductUpdateViewModel, Product>()
				.ForMember(x => x.Reviews, o => o.Ignore())
				.ForMember(x => x.ProductTags, o => o.Ignore())
				.ForMember(x => x.Category, o => o.Ignore())
				.IgnoreAudit();

			CreateMap<Product, ProductUpdateViewModel>();

			CreateMap<IPagedList<Product>, IPagedList<ProductViewModel>>()
				.ConvertUsing<PagedListConverter<Product, ProductViewModel>>();
		}
	}
}