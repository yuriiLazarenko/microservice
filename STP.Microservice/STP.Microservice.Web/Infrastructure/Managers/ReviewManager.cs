﻿using AutoMapper;
using STP.Microservice.Models;
using STP.Microservice.Web.Infrastructure.Factories.Base;
using STP.Microservice.Web.Infrastructure.Managers.Base;
using STP.Microservice.Web.Infrastructure.Validations.Base;
using STP.Microservice.Web.Infrastructure.ViewModels.ReviewModels;

namespace STP.Microservice.Web.Infrastructure.Managers
{
	/// <summary>
	/// Review entity manager
	/// <see cref="IViewModelFactory{TEntity,TCreateViewModel,TUpdateViewModel}"/> implementation
	/// </summary>
	public class ReviewManager : EntityManager<Review, ReviewCreateViewModel, ReviewUpdateViewModel>
	{
		/// <inheritdoc />
		public ReviewManager(IMapper mapper, IViewModelFactory<Review, ReviewCreateViewModel, ReviewUpdateViewModel> viewModelFactory,
			IEntityValidator<Review> validator)
			: base(mapper, viewModelFactory, validator)
		{
		}
	}
}