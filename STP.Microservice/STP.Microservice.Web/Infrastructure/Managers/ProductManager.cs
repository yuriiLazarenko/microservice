﻿using AutoMapper;
using Calabonga.EntityFrameworkCore.UnitOfWork;
using STP.Microservice.Data;
using STP.Microservice.Models;
using STP.Microservice.Web.Controllers;
using STP.Microservice.Web.Infrastructure.Factories.Base;
using STP.Microservice.Web.Infrastructure.Managers.Base;
using STP.Microservice.Web.Infrastructure.Validations.Base;
using STP.Microservice.Web.Infrastructure.ViewModels.ProductViewModels;

namespace STP.Microservice.Web.Infrastructure.Managers
{
	/// <summary>
	/// Product entity manager
	/// <see cref="IViewModelFactory{TEntity,TCreateViewModel,TUpdateViewModel}"/> implementation
	/// </summary>
	public class ProductManager : EntityManager<Product, ProductCreateViewModel, ProductUpdateViewModel>
	{
		private readonly IUnitOfWork<ApplicationDbContext, ApplicationUser, ApplicationRole> _initOfWork;

		/// <inheritdoc />
		public ProductManager(
			IUnitOfWork<ApplicationDbContext,ApplicationUser,ApplicationRole> initOfWork,
			IMapper mapper, 
			IViewModelFactory<Product, ProductCreateViewModel,ProductUpdateViewModel> viewModelFactory,
			IEntityValidator<Product> validator)
			: base(mapper, viewModelFactory, validator)
		{
			_initOfWork = initOfWork;
		}

		public override void OnEditBeforeMappings(ProductUpdateViewModel model, Product entity)
		{
			if (!entity.Visible)
			{
				var catalog = _initOfWork.GetRepository<Category>()
					.GetFirstOrDefault(predicate: x => x.Id == entity.CategoryId);
				if (catalog==null)
				{
					Validator.AddValidationResult(new ValidationResult("Category is not found",true));
				}

				if (catalog != null && !catalog.Visible)
				{
					if (!entity.Visible&&model.Visible)
					{
						Validator.AddValidationResult(new ValidationResult("Cant include product in Catalog, Catalog should be visible",true));
					}
				}
			}
		}
	}
}