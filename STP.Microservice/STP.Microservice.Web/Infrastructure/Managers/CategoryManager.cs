﻿using System.Linq;
using AutoMapper;
using Calabonga.EntityFrameworkCore.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using STP.Microservice.Data;
using STP.Microservice.Models;
using STP.Microservice.Web.Controllers;
using STP.Microservice.Web.Infrastructure.Factories.Base;
using STP.Microservice.Web.Infrastructure.Managers.Base;
using STP.Microservice.Web.Infrastructure.Validations.Base;
using STP.Microservice.Web.Infrastructure.ViewModels.CategoryViewModels;

namespace STP.Microservice.Web.Infrastructure.Managers
{
	public class CategoryManager : EntityManager<Category,CategoryCreateViewModel,CategoryUpdateViewModel>
	{
		private readonly IUnitOfWork<ApplicationDbContext, ApplicationUser, ApplicationRole> _unitOfWork;

		public CategoryManager(
			IUnitOfWork<ApplicationDbContext,ApplicationUser,ApplicationRole> unitOfWork,
			IMapper mapper, 
			IViewModelFactory<Category, CategoryCreateViewModel, CategoryUpdateViewModel> viewModelFactory,
			IEntityValidator<Category> validator) 
			: base(mapper, viewModelFactory, validator)
		{
			_unitOfWork = unitOfWork;
		}

		public override void OnEditBeforeMappings(CategoryUpdateViewModel model, Category entity)
		{
			if (entity.Visible && !model.Visible)
			{
				var products = _unitOfWork.GetRepository<Product>()
					.GetAll()
					.Where(x => x.CategoryId == entity.Id);

				if (products.Any())
				{
					foreach (var p in products)
					{

						p.Visible = false;
						
						_unitOfWork.DbContext.Entry(p).State = EntityState.Modified;
					}
				}
			}
			if (!entity.Visible && model.Visible)
			{
				var products = _unitOfWork.GetRepository<Product>()
					.GetAll()
					.Where(x => x.CategoryId == entity.Id);

				if (products.Any())
				{
					foreach (var p in products)
					{

						p.Visible = true;
						
						_unitOfWork.DbContext.Entry(p).State = EntityState.Modified;
					}
				}
			}
			
		}
	}
}