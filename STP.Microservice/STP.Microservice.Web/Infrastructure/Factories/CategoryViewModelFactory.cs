﻿using System;
using AutoMapper;
using Calabonga.EntityFrameworkCore.UnitOfWork;
using STP.Microservice.Core.Exceptions;
using STP.Microservice.Data;
using STP.Microservice.Models;
using STP.Microservice.Web.Controllers;
using STP.Microservice.Web.Infrastructure.Factories.Base;
using STP.Microservice.Web.Infrastructure.ViewModels.CategoryViewModels;

namespace STP.Microservice.Web.Infrastructure.Factories
{
	public class CategoryViewModelFactory : ViewModelFactory<Category, CategoryCreateViewModel, CategoryUpdateViewModel>
	{
		private readonly IMapper _mapper;
		private readonly IUnitOfWork<ApplicationDbContext, ApplicationUser, ApplicationRole> _unitOfWork;

		/// <inheritdoc />
		public CategoryViewModelFactory(
			IMapper mapper,
			IUnitOfWork<ApplicationDbContext,ApplicationUser,ApplicationRole> unitOfWork)
		{
			_mapper = mapper;
			_unitOfWork = unitOfWork;
		}

		public override CategoryCreateViewModel GenerateForCreate()
		{
			return new CategoryCreateViewModel();
		}

		public override CategoryUpdateViewModel GenerateForUpdate(Guid id)
		{
			//from database get...id
			var category = _unitOfWork.GetRepository<Category>().GetFirstOrDefault(predicate:x => x.Id == id);
			if (category == null)
			{
				throw new MicroserviceArgumentNullException();
			}

			var vieModel = _mapper.Map<CategoryUpdateViewModel>(category);
			return vieModel;
		}
	}
}