﻿using System;
using AutoMapper;
using Calabonga.EntityFrameworkCore.UnitOfWork;
using STP.Microservice.Core.Exceptions;
using STP.Microservice.Models;
using STP.Microservice.Web.Controllers;
using STP.Microservice.Web.Infrastructure.Factories.Base;
using STP.Microservice.Web.Infrastructure.Helpers;
using STP.Microservice.Web.Infrastructure.Services;
using STP.Microservice.Web.Infrastructure.ViewModels.ReviewModels;

namespace STP.Microservice.Web.Infrastructure.Factories
{
	/// <summary>
	/// ViewModel Factory for Review entity
	/// </summary>
	public class ReviewViewModelFactory : ViewModelFactory<Review, ReviewCreateViewModel, ReviewUpdateViewModel>
	{
		private readonly IMapper _mapper;
		private readonly IAccountService _account;
		private readonly IRepository<Review> _repository;

		/// <inheritdoc />
		public ReviewViewModelFactory(IMapper mapper, IRepositoryFactory factory, IAccountService account)
		{
			_mapper = mapper;
			_account = account;
			_repository = factory.GetRepository<Review>();
		}

		/// <inheritdoc />
		public override ReviewCreateViewModel GenerateForCreate()
		{
			var username = "Anonymous";
			var user = AsyncHelper.RunSync(async ()=>await _account.GetCurrentUserAsync());
			if (user!=null)
			{
				username = $"{user.FirstName} {user.LastName}";
			}
			return new ReviewCreateViewModel
			{
				UserName = username
			};
		}

		/// <inheritdoc />
		public override ReviewUpdateViewModel GenerateForUpdate(Guid id)
		{
			var item = _repository.GetFirstOrDefault(predicate: x => x.Id == id);
			if (item == null)
			{
				throw new MicroserviceNotFoundException();
			}

			return _mapper.Map<ReviewUpdateViewModel>(item);
		}
	}
}