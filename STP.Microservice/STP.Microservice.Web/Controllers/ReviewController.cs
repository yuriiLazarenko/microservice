﻿using System;
using System.Threading.Tasks;
using Calabonga.EntityFrameworkCore.UnitOfWork;
using Calabonga.OperationResultsCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using STP.Microservice.Core;
using STP.Microservice.Data;
using STP.Microservice.Models;
using STP.Microservice.Web.Controllers.Base;
using STP.Microservice.Web.Infrastructure.Managers.Base;
using STP.Microservice.Web.Infrastructure.QueryParams;
using STP.Microservice.Web.Infrastructure.Services;
using STP.Microservice.Web.Infrastructure.Settings;
using STP.Microservice.Web.Infrastructure.ViewModels.ProductViewModels;
using STP.Microservice.Web.Infrastructure.ViewModels.ReviewModels;

namespace STP.Microservice.Web.Controllers
{
	/// <summary>
	/// Controller for entity Review with CRUD operations
	/// </summary>
	public class ReviewController : WritableController<Review,
		ReviewCreateViewModel, ReviewUpdateViewModel, ReviewViewModel, DefaultPagedListQueryParams>
	{
		/// <inheritdoc />
		public ReviewController(
			IEntityManager<Review, ReviewCreateViewModel, ReviewUpdateViewModel> entityManager,
			IOptions<CurrentAppSettings> options,
			IUnitOfWork<ApplicationDbContext, ApplicationUser, ApplicationRole> unitOfWork,
			IAccountService accountService)
			: base(entityManager, options, unitOfWork, accountService)
		{
		}

		[Authorize(Roles = AppData.SystemAdministratorRoleName)]
		public override Task<ActionResult<OperationResult<ReviewViewModel>>> DeleteAsync(Guid id)
		{
			return base.DeleteAsync(id);
		}
	}
}