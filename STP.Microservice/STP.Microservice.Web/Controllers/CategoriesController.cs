﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Calabonga.EntityFrameworkCore.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using STP.Microservice.Data;
using STP.Microservice.Models;
using STP.Microservice.Web.Controllers.Base;
using STP.Microservice.Web.Infrastructure.Managers.Base;
using STP.Microservice.Web.Infrastructure.QueryParams;
using STP.Microservice.Web.Infrastructure.Services;
using STP.Microservice.Web.Infrastructure.Settings;
using STP.Microservice.Web.Infrastructure.ViewModels.CategoryViewModels;

namespace STP.Microservice.Web.Controllers
{
    public class CategoriesController : WritableController<Category,CategoryCreateViewModel, CategoryUpdateViewModel, CategoryViewModel, DefaultPagedListQueryParams>
    {
	    public CategoriesController(IEntityManager<Category, CategoryCreateViewModel, CategoryUpdateViewModel> entityManager, IOptions<CurrentAppSettings> options, IUnitOfWork<ApplicationDbContext, ApplicationUser, ApplicationRole> unitOfWork, IAccountService accountService) 
		    : base(entityManager, options, unitOfWork, accountService)
	    {
	    }
    }
}