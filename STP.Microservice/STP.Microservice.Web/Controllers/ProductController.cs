﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Calabonga.EntityFrameworkCore.UnitOfWork;
using Calabonga.OperationResultsCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Options;
using STP.Microservice.Core;
using STP.Microservice.Data;
using STP.Microservice.Models;
using STP.Microservice.Web.Controllers.Base;
using STP.Microservice.Web.Infrastructure.Managers.Base;
using STP.Microservice.Web.Infrastructure.QueryParams;
using STP.Microservice.Web.Infrastructure.Services;
using STP.Microservice.Web.Infrastructure.Settings;
using STP.Microservice.Web.Infrastructure.ViewModels.ProductViewModels;

namespace STP.Microservice.Web.Controllers
{
	/// <summary>
	/// Controller for entity Product with CRUD operations
	/// </summary>
	public class ProductController : WritableController<Product,
		ProductCreateViewModel, ProductUpdateViewModel, ProductViewModel, DefaultPagedListQueryParams>
	{
		/// <inheritdoc />
		public ProductController(IEntityManager<Product, ProductCreateViewModel, ProductUpdateViewModel> entityManager,
			IOptions<CurrentAppSettings> options,
			IUnitOfWork<ApplicationDbContext, ApplicationUser, ApplicationRole> unitOfWork,
			IAccountService accountService)
			: base(entityManager, options, unitOfWork, accountService)
		{
		}

		protected override Func<IQueryable<Product>, IIncludableQueryable<Product, object>> GetIncludes()
		{
			return i => i.Include(x => x.Category);
		}

		[Authorize(Roles = AppData.SystemAdministratorRoleName)]
		public override Task<ActionResult<OperationResult<ProductViewModel>>> DeleteAsync(Guid id)
		{
			return base.DeleteAsync(id);
		}
	}
}