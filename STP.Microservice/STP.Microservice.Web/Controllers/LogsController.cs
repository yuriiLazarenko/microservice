﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Calabonga.EntityFrameworkCore.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using STP.Microservice.Data;
using STP.Microservice.Models;
using STP.Microservice.Web.Controllers.Base;
using STP.Microservice.Web.Infrastructure.QueryParams;
using STP.Microservice.Web.Infrastructure.Services;
using STP.Microservice.Web.Infrastructure.Settings;
using STP.Microservice.Web.Infrastructure.ViewModels.LogViewModels;

namespace STP.Microservice.Web.Controllers
{
    public class LogsController : ReadOnlyController<Log,LogViewModel, PagedListQueryParams>
    {
	    public LogsController(IMapper mapper, IOptions<CurrentAppSettings> options, IUnitOfWork<ApplicationDbContext, ApplicationUser, ApplicationRole> unitOfWork, IAccountService accountService) : base(mapper, options, unitOfWork, accountService)
	    {
	    }
    }
}