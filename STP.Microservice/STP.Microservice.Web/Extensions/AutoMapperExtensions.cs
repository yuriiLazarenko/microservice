﻿using AutoMapper;
using STP.Microservice.Models.Base;

namespace STP.Microservice.Web.Extensions
{
	public static class AutoMapperExtensions
	{
		/// <summary>
		/// Custom extension
		/// </summary>

		/// <summary>
		/// 
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		public static void IgnoreAudit<TSource, TDestination>(this IMappingExpression<TSource, TDestination> source)
				where TDestination : Auditable
			{
				source.ForMember(x => x.CreatedAt, o => o.Ignore());
				source.ForMember(x => x.CreatedBy, o => o.Ignore());
				source.ForMember(x => x.UpdatedAt, o => o.Ignore());
				source.ForMember(x => x.UpdatedBy, o => o.Ignore());
			}
		
	}
}